# Utilities for JohnSear Symfony Bundles
(c) 2019 by [John_Sear](https://bitbucket.org/john_sear-symfony/utilities/)

## Library

This is a Library for the JohnSear Symfony Bundles to use in your symfony application.  
Be sure you are using symfony version 4.4.

> This Bundle is still in Development. Some things can be broken ;-)

## Installation

### via CLI
Run ``composer require jsp/utilities:"0.1.*"`` command in cli to install source with Version 0.1

### composer.json
Add following to your symfony application composer json file:
```json
{
  "require": {
    "jsp/utilities": "0.1.*"
  }
}
```

## Configuration

### Register all Services

For now, all services must be auto wired via the services.yaml.

Add following Lines at the end of ``config\services.yaml``

```yaml
services:

    # ..

    # add more service definitions when explicit configuration is needed
    # please note that last definitions always *replace* previous ones

    JohnSear\Utilities\:
        resource: '../vendor/jsp/utilities/*'
# ..
```

## Additional Documentation
Further Documentation, i.e. Uuid Configuration, can be found [here](Resources/doc/index.md)

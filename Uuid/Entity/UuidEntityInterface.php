<?php declare(strict_types=1);

namespace JohnSear\Utilities\Uuid\Entity;

use JohnSear\Utilities\Uuid\Exception\InvalidUuidException;

interface UuidEntityInterface
{
    /**
     * @throws InvalidUuidException
     */
    public function setId($id): UuidEntityInterface;
    public function getId(): string;
}

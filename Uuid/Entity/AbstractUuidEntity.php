<?php declare(strict_types=1);

namespace JohnSear\Utilities\Uuid\Entity;

use Doctrine\ORM\Mapping as ORM;
use JohnSear\Utilities\Uuid\Exception\InvalidUuidException;
use JohnSear\Utilities\Uuid\Exception\UuidGeneratorException;
use JohnSear\Utilities\Uuid\UuidUtilities;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

abstract class AbstractUuidEntity implements UuidEntityInterface
{
    /**
     * @var UuidInterface
     * @Assert\Uuid(versions={4})
     * @ORM\Id
     * @ORM\Column(type="uuid_binary", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    protected $id;

    /**
     * @throws UuidGeneratorException
     * @throws InvalidUuidException
     */
    public function __construct(string $uuid = null)
    {
        if (empty($uuid)) {
            $uuid = UuidUtilities::uuid4()->toString();
        }
        else {
            UuidUtilities::assertUuid4IsValid($uuid);
        }

        $this->setId($uuid);
    }

    final public function __toString()
    {
        return $this->getDisplayName();
    }

    protected function getDisplayName(): string
    {
        return $this->getId();
    }

    /**
     * @throws InvalidUuidException
     */
    public function setId($id): UuidEntityInterface
    {
        if (is_string($id)) {
            UuidUtilities::assertUuid4IsValid($id);
        }

        $this->id = $id;

        return $this;
    }

    public function getId(): string
    {
        return (string) $this->id;
    }
}

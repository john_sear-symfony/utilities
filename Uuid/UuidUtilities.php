<?php declare(strict_types=1);

namespace JohnSear\Utilities\Uuid;

use Exception;
use JohnSear\Utilities\Uuid\Exception\InvalidUuidException;
use JohnSear\Utilities\Uuid\Exception\UuidGeneratorException;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class UuidUtilities implements UuidUtilitiesInterface
{
    /**
     * @throws UuidGeneratorException
     */
    public function createUuid4(): UuidInterface
    {
        return self::uuid4();
    }

    /**
     * @throws UuidGeneratorException
     */
    public static function uuid4(): UuidInterface
    {
        try {
            return UUID::uuid4();
        } catch (Exception $ex) {
            throw new UuidGeneratorException('Error generating UUID: ' . $ex->getMessage(), 0, $ex);
        }
    }

    /**
     * @throws InvalidUuidException
     */
    public static function assertUuid4IsValid(string $uuid): bool
    {
        if (UUID::isValid($uuid)) {

            return true;
        }

        throw new InvalidUuidException('Uuid "' . $uuid . '" is invalid');
    }

    /**
     * @throws UuidGeneratorException
     */
    public function generateRandomUuid4(int $count = 10): array
    {
        $uuidArray = [];

        for ($i = 1; $i <= $count; $i++) {
            $uuidArray[] = self::uuid4();
        }

        return $uuidArray;
    }
}

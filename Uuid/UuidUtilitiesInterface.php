<?php declare(strict_types=1);

namespace JohnSear\Utilities\Uuid;

use JohnSear\Utilities\Uuid\Exception\InvalidUuidException;
use JohnSear\Utilities\Uuid\Exception\UuidGeneratorException;
use Ramsey\Uuid\UuidInterface;

interface UuidUtilitiesInterface
{
    /**
     * @throws UuidGeneratorException
     */
    public function createUuid4(): UuidInterface;

    /**
     * @throws UuidGeneratorException
     */
    public static function uuid4(): UuidInterface;

    /**
     * @throws InvalidUuidException
     */
    public static function assertUuid4IsValid(string $uuid): bool;
}

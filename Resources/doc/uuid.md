# JohnSear Utilities Documentation - Uuid Entities

Abstract your Entities with AbstractUuidEntity to get Uuid Id setter and getter

## Ramsey Uuid Package Configuration

To use Uuid Entity Abstraction min your Entities, update the ``config\packages\ramsey_uuid_doctrine.yaml`` with following information

```yaml
doctrine:
    dbal:
        types:
            uuid:  Ramsey\Uuid\Doctrine\UuidType
            uuid_binary: Ramsey\Uuid\Doctrine\UuidBinaryType
            uuid_binary_ordered_time: Ramsey\Uuid\Doctrine\UuidBinaryOrderedTimeType
        mapping_types:
            uuid_binary: binary
            uuid_binary_ordered_time: binary
```

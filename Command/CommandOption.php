<?php declare(strict_types=1);

namespace JohnSear\Utilities\Command;

class CommandOption
{
    /**
     * @param string                        $name        The option name
     * @param string|array|null             $shortcut    The shortcuts, can be null, a string of shortcuts delimited by | or an array of shortcuts
     * @param int|null                      $mode        The option mode: One of the InputOption::VALUE_* constants
     * @param string                        $description A description text
     * @param string|string[]|int|bool|null $default     The default value (must be null for InputOption::VALUE_NONE)
     */

    /** @var string */
    private $name = '';
    /** @var string|array|null */
    private $shortcut;
    /** @var int|null */
    private $mode;
    /** @var string */
    private $description = '';
    /** @var string|string[]|int|bool|null */
    private $default;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return array|string|null
     */
    public function getShortcut()
    {
        return $this->shortcut;
    }

    /**
     * @param array|string|null $shortcut
     */
    public function setShortcut($shortcut): self
    {
        $this->shortcut = $shortcut;

        return $this;
    }

    public function getMode(): ? int
    {
        return $this->mode;
    }

    /**
     * @param int|null $mode
     */
    public function setMode(? int $mode): self
    {
        $this->mode = $mode;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return bool|int|string|string[]|null
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * @param bool|int|string|string[]|null $default
     */
    public function setDefault($default): self
    {
        $this->default = $default;

        return $this;
    }
}

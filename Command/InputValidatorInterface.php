<?php declare(strict_types=1);

namespace JohnSear\Utilities\Command;

use Symfony\Component\Console\Exception\InvalidArgumentException;

interface InputValidatorInterface
{
    /**
     * @throws InvalidArgumentException
     */
    public function validateUserInput(string $UserInput): void;
}

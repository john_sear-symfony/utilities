<?php declare(strict_types=1);

namespace JohnSear\Utilities\Command;

use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

interface CommandValidatorInterface
{
    /**
     * @throws InvalidArgumentException
     */
    public function getFlag(InputInterface $input): string;

    /**
     * @throws InvalidArgumentException
     */
    public function validate(InputInterface $input, OutputInterface $output, bool $verbose = true): bool;
}

<?php declare(strict_types=1);

namespace JohnSear\Utilities\Command;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Console\Exception\InvalidArgumentException;

class CommandConfiguration
{
    /** @var string */
    private $name;
    /** @var string */
    private $displayName;
    /** @var string */
    private $description;
    /** @var ArrayCollection */
    private $inputOptions;
    /** @var CommandValidatorInterface */
    private $commandValidator;

    public function __construct()
    {
        $this->inputOptions = new ArrayCollection();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    public function setDisplayName(string $displayName): self
    {
        $this->displayName = $displayName;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return CommandOption[]
     */
    public function getOptions(): array
    {
        return $this->inputOptions->toArray();
    }

    /**
     * Adds an option.
     *
     * @param string                        $name        The option name
     * @param string|array|null             $shortcut    The shortcuts, can be null, a string of shortcuts delimited by | or an array of shortcuts
     * @param int|null                      $mode        The option mode: One of the InputOption::VALUE_* constants
     * @param string                        $description A description text
     * @param string|string[]|int|bool|null $default     The default value (must be null for InputOption::VALUE_NONE)
     *
     * @throws InvalidArgumentException If option mode is invalid or incompatible
     *
     * @return $this
     */
    public function addOption($name, $shortcut = null, $mode = null, $description = '', $default = null): self
    {
        $commandOption = (new CommandOption)
            ->setName($name)
            ->setShortcut($shortcut)
            ->setMode($mode)
            ->setDescription($description)
            ->setDefault($default)
        ;

        if (! $this->inputOptions->contains($commandOption)) {
            $this->inputOptions->add($commandOption);
        }

        return $this;
    }

    public function getCommandValidator(): ? CommandValidatorInterface
    {
        return $this->commandValidator;
    }

    public function setCommandValidator(CommandValidatorInterface $commandValidator): self
    {
        $this->commandValidator = $commandValidator;

        return $this;
    }
}

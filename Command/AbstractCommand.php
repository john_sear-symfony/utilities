<?php declare(strict_types=1);

namespace JohnSear\Utilities\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

abstract class AbstractCommand extends Command
{
    /** @var CommandConfiguration */
    private $commandConfiguration;
    /** @var int */
    private $askingCount = 0;
    /** @var ConsoleLogger */
    private $logger;

    public function __construct(CommandConfiguration $commandConfiguration)
    {
        $this->setCommandConfiguration($commandConfiguration);

        parent::__construct();
    }

    protected function getLogger(): ConsoleLogger
    {

        return $this->logger;
    }

    protected function log($level, $message, array $context = []): void
    {
        $this->getLogger()->log($level, $message, $context);
    }

    public function getCommandConfiguration(): CommandConfiguration
    {
        return $this->commandConfiguration;
    }

    public function setCommandConfiguration(CommandConfiguration $commandConfiguration): self
    {
        $this->commandConfiguration = $commandConfiguration;

        return $this;
    }

    /**
     * @throws InvalidArgumentException
     */
    protected function configure(): void
    {
        $commandConfiguration = $this->getCommandConfiguration();
        $this->setName($commandConfiguration->getName());

        $this->setDescription($commandConfiguration->getDescription());

        foreach ($commandConfiguration->getOptions() as $option) {
            $this->addOption(
                $option->getName(),
                $option->getShortcut(),
                $option->getMode(),
                $option->getDescription(),
                $option->getDefault()
            );
        }
    }

    abstract protected function executeActions(InputInterface $input, OutputInterface $output);

    /**
     * @throws InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $this->logger = new ConsoleLogger($output);

        $commandConfiguration = $this->getCommandConfiguration();
        $commandValidator = $commandConfiguration->getCommandValidator();
        if (! $commandValidator instanceof CommandValidatorInterface) {
            $this->writeError($output, 'No valid Command Validator set.');
            return;
        }

        $this->writeHeader($output);
        if ($commandValidator->validate($input, $output)) {
            $this->executeActions($input, $output);
            $this->writeFooter($output);
        }
    }

    /**
     * @throws InvalidArgumentException
     */
    private function writeHeader(OutputInterface $output): void
    {
        $output->writeln('');

        $output->writeln('<fg=magenta>==><fg=default> ' . $this->getCommandConfiguration()->getDisplayName() . ' <fg=magenta><==<fg=default>');
    }

    /**
     * @throws InvalidArgumentException
     */
    private function writeFooter(OutputInterface $output): void
    {
        $output->writeln(['<info>...all done!</info>', '']);
    }

    protected function writeError(OutputInterface $output, string $errorMessage): void
    {
        $output->writeln('');
        $output->writeln('<fg=red>' . $errorMessage . '<fg=default>');
        $output->writeln('');
    }

    public function askFor(InputInterface $input, OutputInterface $output, string $question, string $default = '', string $help = '', int $retryCount = 3, InputValidatorInterface $inputValidator = null): string
    {
        if ($help !== '') {
            $output->writeln($help);
        }

        $defaultString = ($default !== '') ? ' [<comment>' . $default . '</comment>]' : '';

        $helper = $this->getHelper('question');
        $questionObj = new Question($question . $defaultString . ': ', $default);
        $userInput = $helper->ask($input, $output, $questionObj);

        if ($inputValidator instanceof InputValidatorInterface) {
            try {
                $inputValidator->validateUserInput($userInput);
            } catch (InvalidArgumentException $ex) {
                $errorMessage = 'Input not valid:<fg=default> ' . $ex->getMessage();
                $this->writeError($output, $errorMessage);

                $userInput = '';
            }
        }

        if ($userInput === '' && $retryCount > 0) {
            $this->askingCount++;
            if ($this->askingCount <= $retryCount) {
                $output->writeln('Please try again.');
                $output->writeln('');
                if ($this->askingCount > 0) {
                    $output->writeln('(Retry No. ' . $this->askingCount . '/' . $retryCount . ')');
                }
                $userInput = $this->askFor($input, $output, $question, $default, $help, $retryCount, $inputValidator);
            }

            return $userInput;
        }

        $this->askingCount = 0;

        return $userInput;
    }
}
